#! /bin/bash

set -e

if test -z "$INSTALLDIR"; then
    echo "INSTALLDIR not set"
    exit 1
fi

TARGET=$INSTALLDIR/list.html

cat list-top > $TARGET

for GEP in gep-*.html; do 
    xsltproc -html -stringparam filename $GEP list.xsl $GEP >> $TARGET
done

cat list-bottom >> $TARGET
