<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns="http://www.w3.org/TR/xhtml1/transitional"
                exclude-result-prefixes="#default">

  <xsl:output method="text" encoding="utf-8"/>

  <xsl:param name="filename" value="'foo.html'"/>
  <xsl:template match="/">
    <xsl:variable name="title" select="html/head/title"/>
    <xsl:variable name="owner" select="html/body/dl[1]/dd[@class='owner']/a"/>
    <xsl:variable name="start" select="html/body/dl[1]/dd[@class='start']"/>
    <xsl:variable name="end" select="html/body/dl[1]/dd[@class='end']"/>
    <xsl:variable name="status" select="html/body/dl[1]/dd[@class='status']"/>

    <xsl:text>&lt;tr&gt;&#10;</xsl:text>

    <!-- title plus link -->
    <xsl:text>  &lt;td&gt;&lt;a href="</xsl:text>
    <xsl:value-of select="$filename"/>
    <xsl:text>"&gt;</xsl:text>
    <xsl:value-of select="$title"/>
    <xsl:text>&lt;/a&gt;&lt;/td&gt;&#10;</xsl:text>

    <!-- end of discussion period -->
    <xsl:text> &lt;td&gt; </xsl:text>
    <xsl:value-of select="$end"/>
    <xsl:text> &lt;/td&gt;&#10; </xsl:text>

    <!-- status (Pending/Accepted/etc) -->
	<xsl:choose>
		<xsl:when test="$status='Pending'">
	    	<xsl:text>  &lt;td bgcolor="#CCFFCC"&gt;</xsl:text>
		</xsl:when>
		<xsl:when test="$status='Rejected'">
	    	<xsl:text>  &lt;td bgcolor="#FFCCCC"&gt;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
    		<xsl:text>  &lt;td&gt;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
    <xsl:value-of select="$status"/>
    <xsl:text>&lt;/td&gt;&#10;</xsl:text>

    <!-- owner of GEP -->
    <xsl:text>  &lt;td&gt;</xsl:text>
    <xsl:value-of select="$owner"/>
    <xsl:text>&lt;/td&gt;&#10;</xsl:text>

    <xsl:text>&lt;/tr&gt;&#10;</xsl:text>

  </xsl:template>

</xsl:stylesheet>
