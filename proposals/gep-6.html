<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>REQUIREMENTS GEP 6: Toolbar</title>
<link rel="stylesheet" href="gep.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>

<h1>REQUIREMENTS GEP 6: Toolbar</h1>

<p>The current <code>GtkToolbar</code> widget lacks a number of
features which are desirable to applications.  This GEP discusses the
requirements for an updated toolbar widget.</p>

<h2>1. Administrivia</h2>

<dl>
<dt>Document Owner</dt>
<dd class="owner"><a href="mailto:james@daa.com.au">James Henstridge</a></dd>

<dt>Posted</dt>
<dd class="start">September 12, 2002</dd>

<dt>Discussion Period Ends</dt>
<dd class="end">October 1, 2002</dd> <!-- must be at least 1 week after posted date -->

<dt>Status</dt>
<dd class="status">Pending</dd> <!-- moves to Approved or Rejected -->

<dt>Discussion List</dt>
<dd class="list"><a href="mailto:gtk-devel-list@gnome.org">gtk-devel-list@gnome.org</a></dd>

<dt>Responsible Persons</dt>
<dd>
<!-- at least three! -->
<a href="mailto:james@daa.com.au">James Henstridge</a>, 
<a href="mailto:andersca@gnu.org">Anders Carlsson</a>,
<a href="mailto:otaylor@redhat.com">Owen Taylor</a>,
<a href="mailto:michael@ximian.com">Michael Meeks</a>
</dd>

</dl>

<h2>2. Requirements</h2>

<p>Due to a number of inadequacies, the <code>GtkToolbar</code> widget
is passed over for alternatives (such as <code>BonoboUIToolbar</code>)
in various programs.  There are a number of down sides to this
arrangement:</p>

<ul>
  <li>The toolbars on different applications act differently.</li>
  <li>Keyboard navigation and accessibility may not be implemented to
  the same level as <code>GtkToolbar</code>, or may be
  inconsistent.</li>
  <li><em>think of some more reasons why this is bad</em></li>
</ul>

<p>Some of the reasons why replacement toolbars have been developed
include:</p>

<ul>
  <li>Toolbar buttons are difficult to manipulate.  As they are not
  widgets in their own right (just a structure pointing at the
  <code>GtkButton</code>, label and icon that make them up), it is
  difficult to move buttons within a toolbar, or between
  toolbars.</li>

  <li>No way to specify that a toolbar item should expand to fill
  available space (eg. the location bar in Nautilus), or be
  right aligned (eg. a throbber).</li>

  <li>Separators are not quite like other toolbar items.  On
  occasions, you might want to hide some buttons on the toolbar.
  However, there is no way to hide the separators other than removing
  them.  This could be solved by making them widgets like the other
  items.</li>

  <li>Overflow is not handled.  The current <code>GtkToolbar</code>
  sets its requested minimum width (or height for vertical toolbars)
  wide enough to display all items.  In some cases, it is desirable to
  allocate less space to the toolbar (eg. small screens, or the user
  resizing the window).  It would be nice to have the toolbar handle
  the overflowing items.</li>

  <li>Difficult to implement toolbar customisation in terms of the
  existing API.</li>

  <li>No support for "priority text" mode.  For some applications, it
  is desirable to display text labels next to some icons, but not
  others.  This is done on a number of Windows applications, and
  Evolution.</li>
</ul>

<p>From these experiences, we can put together a list of requirements
for an updated toolbar:</p>

<ol>
  <li>Must provide consistent handling of different children types,
  including buttons, separators and other widgets.</li>
  <li>Must provide packing options for items, including "expand" and
  "pack end" behaviour.</li>
  <li>Must handle overflow items.</li>
  <li>Must support "priority text" mode.</li>
  <li>Must meet accessibility and keyboard navigation
  requirements and work in right to left environments.</li>
  <li>Must provide enough features to satisfy Bonobo's needs.</li>
  <li>Should make toolbar customisation possible/easy.</li>
  <li>Should stay compatible with the existing GtkToolbar widget.</li>
</ol>

<p>A sample implementation is being maintained in the <code><a
href="http://cvs.gnome.org/lxr/source/libegg/libegg/toolbar/"
>libegg</a></code> module, and will be updated as the GEP
progresses.</p>


<h3>2.1 Toolbar Children as Widgets</h3>

<p>Unlike other container widgets in GTK, <code>GtkToolbar</code>
provides functions that create and add toolbar items to the toolbar in
a single step.  This is in contrast to other container widgets, where
children are constructed first, then added to the parent.</p>

<p>This is because toolbar items are generic <code>GtkButton</code>
widgets with some callbacks attached.  The only way the callbacks can
be attached is by creating the button with one of the provided
<code>GtkToolbar</code> APIs.</p>

<p>As the widget has evolved, the number of methods used to add items
has grown to 17.</p>

<p>By having specialised toolbar child widgets that managed their own
appearance (in contrast to the way <code>GtkToolbar</code> currently
maintains it), we could reduce the number of APIs substantially.
Other than the standard <code>GtkContainer</code> <code>add</code> and
<code>remove</code> method, we could make do with an
<code>insert</code> method something like this:</p>

<blockquote><pre>
void gtk_toolbar_insert_toolitem (GtkToolbar  *toolbar,
                                  GtkToolItem *item,
                                  gint         position);
</pre></blockquote>

<p>Such a function could be used for <code>append</code> and
<code>prepend</code> operations, by passing -1 or 0 for the
<code>position</code> argument respectively.  If desired, actual
functions could be provided as well.

<p>By treating separators as toolbar items too, we get rid of the need
for special APIs to add/remove them, and can manipulate them as with
any other item (changing the visibility, in particular).


<h3>2.2 Packing Options</h3>

<p>There are a number of real world examples where "expand" and "pack
end" options would be useful.</p>

<p>The most obvious use for an "expand" option is for things like the
Mozilla and Nautilus toolbars, where the location entry box should
expand to fill any extra space.</p>

<p>The "pack end" option is useful for toolbar items such as
throbbers/spinners found in web browsers.</p>


<h3>2.3 Overflow</h3>

<p>On some displays, a toolbar may not be able to display all items in
the width of the screen.  The toolbar should handle this gracefully,
rather than forcing the window to be wider than the screen.  The
common way to handle this is to omit the last items in the toolbar and
provide a small arrow button at the end of the toolbar that can be
used to access the extra items.</p>

<p>Two methods of presenting the additional toolbar items include:</p>

<ul>
  <li>The <code>BonoboUIToolbar</code> method, which pops up a panel
  containing the extra toolbar items.</li>
  <li>The method used on Qt, Windows and Mac OS X, where a menu is
  popped up that contains menu items representing the extra
  items.</li>
</ul>

<p>The <code>BonoboUIToolbar</code> method has the benefit of not
requiring additional support from toolbar items.  In contrast, the
second is more consistent with other popular user interfaces so has
the benefit of familiarity.</p>

<p>Final say on how overflow items should be presented should probably
be made by the usability team.</p>


<h3>2.4 Priority Text</h3>

<p>For some toolbars, the number of items makes it impractical to
display text labels for all items, as it would cause many items to
overflow.  However, it may be desirable to display labels for some
items.</p>

<p>This is achieved by another toolbar style known as "priority text"
mode in the <code>BonoboUIToolbar</code> code.  In this mode, most
items only display an icon, while some display a text label next to
the icon.  This makes it easier to identify important toolbar items,
and provides a larger target for use with the mouse.</p>


<h3>2.5 Accessibility and Internationalisation Requirements</h3>

<p>The updated toolbar should meet accessibility requirements.  The
main one that must be implemented within the toolbar code itself is
keyboard navigation.  The draft set of key bindings for toolbars is
available at the GNOME Accessibility Project web site:</p>

<blockquote>
<a
href="http://developer.gnome.org/projects/gap/keynav/gtk_menus.html"
>http://developer.gnome.org/projects/gap/keynav/gtk_menus.html</a>
</blockquote>

<p>On the i18n front, the toolbar should work well in right-to-left
environments, like other GTK widgets.  This includes:</p>

<ul>
  <li>laying out items from the right edge of the toolbar</li>
  <li>packing "pack end" items at the left edge</li>
  <li>when in "both_horiz" or "priority text" toolbar modes, display
  text labels to the left of their icons.
</ul>


<h3>2.6 Satisfy Bonobo's Requirements</h3>

<p>The <code>BonoboUIToolbar</code> widget was developed because the
<code>GtkToolbar</code> widget could not do everything that was
needed.  It is desirable that the updated toolbar widget be satisfy
Bonobo's requirements, so that the <code>BonoboUIToolbar</code> widget
can be deprecated in a future version.</p>

<p>The main requirement not mentioned elsewhere is the ability to
insert Bonobo controls into a toolbar.  This should be possible to do
through the use of a toolbar item wrapper around the control.</p>


<h3>2.7 Toolbar Customisation</h3>

<p>Customisable toolbars are a desirable feature for many
applications.  Having toolbar items as first class objects that can be
added and removed from toolbars should make implementation of this
feature a lot easier.</p>

<p>There are three main toolbar customisation user interfaces in use
in modern applications:</p>

<dl>
  <dt><b>Direct manipulation</b></dt>
  <dd>As in Microsoft Office.  Toolbar items are dragged from a
  palette onto the actual toolbars.  To rearrange items, they are
  simply dragged to the new position.  To remove items, they are
  dragged back to the palette.  This method is good for customising
  multiple toolbars at once.</dd>

  <dt><b>Mac OS X Style</b></dt>
  <dd>A copy of the toolbar and a palette of toolbar items are
  displayed in a single dialog.  This interface is better suited for
  single toolbars.</dd>

  <dt><b>Internet Explorer Style</b></dt>
  <dd>This is another user interface suitable for customising a single
  toolbar.  It has a dialog with two list boxes: one of available
  toolbar items, and another with the toolbar items currently on the
  toolbar.  There are buttons for moving items up and down in the
  list, and to move items between the two lists.  Items can also be
  added/removed with drag and drop.</dd>
</dl>

<p>Decision on the preferred interface should be up to the usability
project and/or accessibility project.</p>

<p>Whether the toolbar should include code to help customise the
toolbar or not is open to discussion.  The toolbar should definitely
not get in the way of such code though.</p>


<h3>2.8 Compatibility Concerns</h3>

<p>Compatibility with the existing <code>GtkToolbar</code> is
desirable, as it eliminates the need to deprecate the widget.</p>

<p>By using specialised widgets in the toolbar, the existing methods
used to create toolbar buttons could be deprecated in favour of a
simpler API, as outlined in Section 2.1.</p>

<p>Although no fields are marked with
<tt>/*&lt;&nbsp;public&nbsp;&gt;*/</tt>, the <code>num_children</code>
and <code>children</code> fields are mentioned in the GTK
documentation.  These members appear to be used by AbiWord, so can not
be ignored.  It should be fairly easy to write compatibility code in
the deprecated toolbar item creation code.  If an app uses the new
APIs, the contents of these members would not be defined.</p>

<p>Additional fields could be stored in a private structure accessed
with <code>g_object_get_data()</code>, so as to not grow the structure
size.</p>


<h2>3. Issues Raised During Discussion</h2>

<p>None yet.</p>


<h2>4. Decision and Rationale</h2>

<p>None yet.</p>


<h2>5. Amendments and Clarifications</h2>

<p>None yet.</p>


</body>
</html>
